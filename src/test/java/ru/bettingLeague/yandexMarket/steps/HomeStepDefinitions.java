package ru.bettingLeague.yandexMarket.steps;

import cucumber.api.java.ru.И;
import ru.bettingLeague.yandexMarket.pages.HomePage;

public class HomeStepDefinitions {
    HomePage homePage = new HomePage();

    @И("^пользователь нажимает на элемент Электроника на домашней странице$")
    public void clickElementOnHomePage() {
        homePage.clickElementOnHomePage();
    }

}

package ru.bettingLeague.yandexMarket.steps;

import cucumber.api.java.ru.И;
import ru.bettingLeague.yandexMarket.actions.ActionsImpl;

public class ActionsStepDefinitions{
    ActionsImpl actions = new ActionsImpl();

    @И("^пауза (\\d+) секунд$")
    public void wait(int timeSleep) {
        actions.waitLoading(timeSleep);
    }

    @И("^пользователь скроллит на верх страницы$")
    public void scrollToTopPage() {
        actions.scrollToTopPage();
    }
}

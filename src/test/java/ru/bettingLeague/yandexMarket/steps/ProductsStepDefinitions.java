package ru.bettingLeague.yandexMarket.steps;

import cucumber.api.java.ru.И;
import ru.bettingLeague.yandexMarket.pages.ProductsPage;

public class ProductsStepDefinitions {
    ProductsPage productsPage = new ProductsPage();

    @И("^пользователь нажимает на элемент Правое меню->Производитель->LG на странице телевизоры$")
    public void clickLGProducer() {
        productsPage.clickLGProducer();
    }

    @И("^пользователь нажимает на элемент Правое меню->Производитель->Samsung на странице телевизоры$")
    public void clickSamsungProducer() {
        productsPage.clickSamsungProducer();
    }

    @И("^пользователь скроллит до элемента Правое меню->Цена от на странице телевизоры$")
    public void scrollToElement() {
        productsPage.scrollToElement();
    }

    @И("^пользователь заполняет поле Правое меню->Цена от значением \"([^\"]*)\" на странице телевизоры$")
    public void fillField(String value) {
        productsPage.fillField(value);
    }

    @И("^пользователь выбирает первый телевизор из списка на странице телевизоры$")
    public void clickFirstTV() {
        productsPage.clickFirstTV();
    }
}

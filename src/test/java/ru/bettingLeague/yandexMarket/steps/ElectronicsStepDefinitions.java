package ru.bettingLeague.yandexMarket.steps;

import cucumber.api.java.ru.И;
import ru.bettingLeague.yandexMarket.pages.ElectronicsPage;

public class ElectronicsStepDefinitions {
    ElectronicsPage electronicsPage = new ElectronicsPage();

    @И("^пользователь нажимает на элемент Левое меню->Телевизоры на странице электроника$")
    public void clickElementOnHomePage() {
        electronicsPage.clickElementOnElectronicsPage();
    }

    @И("^пользователь скроллит до элемента Левое меню->Телевизоры на странице электроника$")
    public void scrollToElement() {
        electronicsPage.scrollToElement();
    }
}

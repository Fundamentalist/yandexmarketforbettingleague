package ru.bettingLeague.yandexMarket.steps;

import cucumber.api.java.ru.И;
import io.cucumber.datatable.DataTable;
import ru.bettingLeague.yandexMarket.pages.TVPage;

public class TVStepDefinitions {
    TVPage tvPage = new TVPage();

    @И("^пользователь проверяет, что заголовок телевизора содержит:$")
    public void checkTitleContains(DataTable dataTable) {
        tvPage.checkTitleContains(dataTable);
    }

    @И("^пользователь проверяет, что цена больше или равна (\\d+) на странице телевизор$")
    public void checkPrice(int priceValue) {
        tvPage.checkPrice(priceValue);
    }
}

package ru.bettingLeague.yandexMarket.steps;

import cucumber.api.java.ru.И;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchTo;

public class CommonStepDefinitions {

    @И("^пользователь открывает \"([^\"]*)\"$")
    public void openWebPortal(String url) {
        open(url);
    }

    @И("^пользователь переключается на вторую вкладку$")
    public void switchToSecondTab() {
        switchTo().window(1);
    }

}

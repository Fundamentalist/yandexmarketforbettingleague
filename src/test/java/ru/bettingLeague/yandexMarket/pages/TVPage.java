package ru.bettingLeague.yandexMarket.pages;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.datatable.DataTable;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class TVPage {

    private final SelenideElement title = $("[data-apiary-widget-name=\"@MarketNode/ProductCardTitle\"] h1");
    private final SelenideElement price = $("[data-autotest-currency=\"₽\"]");

    public void checkTitleContains(DataTable dataTable){
        boolean res = false;
        for(String tvTitle: dataTable.asList()){
            if(title.getText().contains(tvTitle)){
                res = true;
            }
        }
        if(!res){
            throw new AssertionError("Выбранный продукт не содержит необходимой марки телевизора");
        }
    }

    public void checkPrice(int priceValue){
        String conversionPrice = getConversionPrice(price.getText());
        if(Integer.parseInt(conversionPrice) < priceValue){
            throw new AssertionError(String.format("Цена продукта %s меньше ожидаемого %s", price.getText(), priceValue));
        }
    }

    private String getConversionPrice(String price){
        return price.substring(0, price.indexOf(" ₽")).replace(" ", "");
    }
}

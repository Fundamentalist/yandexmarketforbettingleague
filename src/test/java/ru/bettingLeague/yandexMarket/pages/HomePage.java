package ru.bettingLeague.yandexMarket.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HomePage {

    private final SelenideElement electronics = $("[href=\"/catalog--elektronika/54440\"] span");

    public void clickElementOnHomePage(){
        electronics.click();
    }

}

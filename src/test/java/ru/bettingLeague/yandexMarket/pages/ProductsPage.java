package ru.bettingLeague.yandexMarket.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.actions;

public class ProductsPage {

    private final SelenideElement priceFrom = $("[name=\"Цена от\"]");
    private final SelenideElement LGProducer = $("[name=\"Производитель LG\"] + div span");
    private final SelenideElement SamsungProducer = $("[name=\"Производитель Samsung\"] + div span");

    public void clickLGProducer(){
        LGProducer.click();
    }

    public void clickSamsungProducer(){
        SamsungProducer.click();
    }

    public void scrollToElement(){
        actions().moveToElement(priceFrom);
    }

    public void fillField(String value){
        priceFrom.setValue(value);
    }

    public void clickFirstTV(){
        $("[data-zone-name=\"snippetList\"] img").click();
    }
}

package ru.bettingLeague.yandexMarket.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.actions;

public class ElectronicsPage {

    private final SelenideElement tvSection = $("[href=\"/catalog--televizory/18040671/list?hid=90639\"]");

    public void clickElementOnElectronicsPage(){
        tvSection.click();
    }

    public void scrollToElement(){
        actions().moveToElement(tvSection);
    }
}

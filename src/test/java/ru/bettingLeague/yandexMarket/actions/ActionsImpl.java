package ru.bettingLeague.yandexMarket.actions;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class ActionsImpl {

    public void scrollDown(int pxValue) {
        Selenide.executeJavaScript("window.scrollBy(0," + pxValue + ")", "");
    }

    public void scrollUp(int pxValue) {
        Selenide.executeJavaScript("window.scrollBy(0,-" + pxValue + ")", "");
    }

    public void waitLoading(int seconds) {
        Selenide.sleep((long) 1000*seconds);
    }

    public void scrollToTopPage(){
        Selenide.executeJavaScript("window.scrollTo(0,0)", "");
    }

    public void moveToElement(SelenideElement selenideElement) {
        Selenide.actions().moveToElement(selenideElement).perform();
    }
}
